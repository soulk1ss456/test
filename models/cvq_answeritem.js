"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class cvq_answeritem extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // console.log(models);
      // cvq_answeritem.hasMany(models.cvq_quiz)
      // define association here
    }
  }
  cvq_answeritem.init(
    {
      QuizID: {
        type: DataTypes.INTEGER,
        primaryKey: true,
      },
      QuestionItemNo: {
        type: DataTypes.INTEGER,
        primaryKey: true,
      },
      AnswerItemNo: {
        type: DataTypes.INTEGER,
        primaryKey: true,
      },
      AnswerText: DataTypes.STRING,
      AnswerType: DataTypes.STRING,
      ScoreValue: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "cvq_answeritem",
      tableName: "cvq_answeritem",
      timestamps: false,
    }
  );
  return cvq_answeritem;
};
