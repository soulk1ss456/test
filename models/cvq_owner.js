"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class cvq_owner extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  cvq_owner.init(
    {
      OwnerID: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      OwnerName: DataTypes.STRING,
      OwnerContact: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "cvq_owner",
      tableName: "cvq_owner",
      timestamps: false,
    }
  );
  return cvq_owner;
};
