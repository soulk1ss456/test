"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class cvq_questionitem extends Model {
    static associate(models) {
      // define association here
    }
  }
  cvq_questionitem.init(
    {
      QuizID: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      QuestionItemNo: {
        type: DataTypes.INTEGER,
        primaryKey: true,
      },
      QuestionText: DataTypes.STRING,
      QuestionPart: DataTypes.STRING,
      QuestionType: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "cvq_questionitem",
      tableName: "cvq_questionitem",
      timestamps: false,
    }
  );
  return cvq_questionitem;
};
