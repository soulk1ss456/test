"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class cvq_quiz extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      cvq_quiz.hasOne(models.cvq_owner, {
        foreignKey: "OwnerID",
      });
      // define association here
    }
  }
  cvq_quiz.init(
    {
      QuizID: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      QuizAttribute: DataTypes.STRING,
      OwnerID: DataTypes.INTEGER,
      QuizURL: DataTypes.STRING,
      OrderPriority: DataTypes.INTEGER,
      DoAgainDayInterval: DataTypes.INTEGER,
      Status: DataTypes.STRING,
    },
    {
      sequelize,
      modelName: "cvq_quiz",
      tableName: "cvq_quiz",
      timestamps: false
    }
  );
  return cvq_quiz;
};
