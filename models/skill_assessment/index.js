const { query } = require("../../config/connection");

module.exports = {
  get_type_quiz: async () => {
    const sql =
      "SELECT q.QuizID, o.OwnerName, o.OwnerContact, q.QuizAttribute, q.DoAgainDayInterval FROM cvq_quiz AS q INNER JOIN cvq_owner AS o ON q.OwnerID = o.OwnerID WHERE Status = 'Active'";
    const result = await query(sql);
    return result;
  },
 
};
