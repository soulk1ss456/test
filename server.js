const express = require("express");
const cors = require("cors");
const helmet = require("helmet");
const app = express();
const apis = require("./router");
const morgan = require("morgan");
const fs = require("fs");
const path = require("path");


// ------------------------------------- middleware -----------------------------
require("./config/passport");

app.use(express.json());
app.use(
  express.urlencoded({
    extended: true,
  })
);

// const accessLogStream = fs.createWriteStream(
//   path.join(__dirname, "access.log"),
//   { flags: "a" }
// );

// app.use(morgan("dev", { stream: accessLogStream }));
app.use(morgan("dev"));
app.use(helmet());
app.use(cors());

app.use(function (req, res, next) {
  // Website you wish to allow to connect
  res.setHeader("Access-Control-Allow-Origin", "*");

  // Request methods you wish to allow
  res.setHeader(
    "Access-Control-Allow-Methods",
    "GET, POST"
  );

  // Request headers you wish to allow
  res.setHeader(
    "Access-Control-Allow-Headers",
    "X-Requested-With,content-type"
  );

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader("Access-Control-Allow-Credentials", true);

  // Pass to next layer of middleware
  next();
});

// ----------------------------------------- middleware -----------------------------
app.use("/api", apis);

const port = 5000 || process.env.PORT;

app.listen(port, () => {
  console.log(`server listen at port ${port}`);
});
