function errorMiddleware(error, res) {
    let { status = 500, message, data } = error;
  
    console.log(`[Error] ${error}`);
  
    message = status === 500 || !message ? "Internal server error" : message;
  
    error = {
      type: "error",
      status,
      message,
      ...(data && data),
    };
  
    res.status(status).send(error).end();
  }
  
  module.exports = errorMiddleware;