// const models = require('../../models') use Sequelize
const quiz_model = require("../../models/skill_assessment");
const { errorMiddleware } = require("../../middleware");

module.exports = {
  get_all: async (req, res) => {
    try {
      const result = await quiz_model.get_quiz();
      return res.status(200).json(result).end();
    } catch (error) {
      return errorMiddleware(error, res);
    }
  },
};
