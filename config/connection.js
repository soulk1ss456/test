const env = process.env.NODE_ENV;
const config = require(__dirname + "/../config/config.json")[env];
const mysql = require("mysql2");

module.exports = {
  query: async (sql, value) => {
    return new Promise((resolve, reject) => {
      const callback = (error, result) => {
        connection.end();
        if (error) {
          console.log("[MYSQL] Error connecting to mysql:" + error + "\n");
          reject(error);
          return;
        }
        resolve(result);
      };
      const connection = mysql.createConnection(config);
      connection.connect(function (err, con) {
        if (err) {
          console.log("[MYSQL] Error connecting to mysql:" + err + "\n");
          reject(err);
          return;
        }
      });
      connection.query(sql, value, callback);
    }).catch((err) => {
      throw err;
    });
  },
};
