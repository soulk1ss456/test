'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('cvq_quiz', {
      QuizID: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      QuizAttribute: {
        type: Sequelize.STRING
      },
      OwnerID: {
        type: Sequelize.INTEGER
      },
      QuizURL: {
        type: Sequelize.STRING
      },
      OrderPriority: {
        type: Sequelize.INTEGER
      },
      DoAgainDayInterval: {
        type: Sequelize.INTEGER
      },
      Status: {
        type: Sequelize.STRING
      },
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('cvq_quiz');
  }
};