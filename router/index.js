const express = require("express");
const router = express.Router();
const passport = require("passport");
const quiz_router = require("./skill_assessment");

router.use(
  "/quiz",
  // passport.authenticate("jwt", { session: false }),
  quiz_router
);

module.exports = router;
