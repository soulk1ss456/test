const express = require("express");
const router = express.Router();

const quiz_controller = require("../../controller");

router.get("/", quiz_controller.get_all);

module.exports = router;
